
var thermoaulas_path = "http://mikrogen.tknika.net/thermoaulas-data/" ; //"/usr/share/thermoaulas/" ;
var rooms_file = thermoaulas_path + "rooms" ;
var heating_curves_file = thermoaulas_path + "heatingcurves" ;
var heating_curve_file = thermoaulas_path + "data/heatingcurve_" ;
var conf_file = thermoaulas_path + "configuration.properties" ;

var rooms_file_relative_path = "rooms" ;
var heating_curve_file_relative_path = "data/heatingcurve_" ;
var configuration_file_relative_path = "configuration.properties" ;

var FILES_MANAGER_SERVICE_URL="http://mikrogen.tknika.net/tknika/save_to_disk.php"

var num_points_in_curve = 200 ;
var graphShown = false ;

var rooms = [];

$(function() {
    load_rooms();
    
    // Bind events
    $("#select-room").change(room_selected) ;
    $("#select-room").click(room_selected) ;
    $("#btn-save").click(save_room);
    $("#btn-remove").click(remove_room);
    $("#btn-clear").click(clear_form);


    
    
    
});

function load_rooms() {
	$.get(rooms_file, function(data) {
        rooms = [] ;
        var lines = data.split("\n");

        $.each(lines, function(n, elem) {
        	// Nafarroa Aretoa,nafarroa103,tknika.net_2d38323338383934302d353231@resource.calendar.google.com,20
        	var items = elem.split(",") ;
        	if(items && items.length >= 4) {
        		var room = {} ;
        		room.name = items[0] ;
        		room.openhab_id = items[1] ;
        		room.calendar_id = items[2] ;        		
        		room.ref_temp = items[3] ;
        		room.target = items[4].trim();
        		load_curve_heating(room) ; 
        		
        		rooms.push(room) ;
        		
        	}
        });
        
        // load form control
        
        $.each(rooms, function(n, elem) {
        	$("<option value='" + n + "'>" + elem.name +  "</option>").appendTo("#select-room");
        });
        
        
    
    });
}

function room_selected() {	
	load_room($("#select-room").val());	
	load_graph(rooms[$("#select-room").val()]) ;	
}

function load_room(room_index) {
	
	if(!room_index)
		room_index=0 ;
	
	if(room_index >= rooms.length)
		return ;
	
	var room = rooms[room_index] ;
	
	
	$("#room-name").val(room.name) ;
	$("#room-calendar-id").val(room.calendar_id) ;
	$("#room-openhab-id").val(room.openhab_id) ;
	$("#room-ref-temp").val(room.ref_temp) ;
	
	document.getElementById("room-target").selectedIndex = getIndexOfTarget(room.target);
	
	
}

function getIndexOfTarget(room_target) {
	if(room_target=="electricity")
		return 0;
	if(room_target=="heating")
		return 1;
	if(room_target=="all")
		return 2;
	return -1 ;	
}

function save_room() {
	var room = {} ;
	
	room.name = $("#room-name").val() ;
	room.calendar_id = $("#room-calendar-id").val() ;
	room.openhab_id = $("#room-openhab-id").val() ;
	room.ref_temp = $("#room-ref-temp").val() ;
	room.target = $("#room-target").val();
	
	if(!room.name || !room.calendar_id || !room.openhab_id || !room.ref_temp ) {
		alert("Faltan datos para crear el aula") ;
		return
	}
	
	room.name = room.name.trim();
	room.calendar_id = room.calendar_id.trim();
	room.openhab_id = room.openhab_id.trim();
	room.ref_temp = room.ref_temp.trim();	
	room.target = room.target.trim();
	
	for( var i=0; i < rooms.length; i++) {
		// update existing room
		if( rooms[i].name == room.name ) {
			rooms[i] = room;
			save_rooms();
			location.reload();
			return ;
		}
	}
	
	// add new room
	rooms.push(room) ;
	load_curve_heating(room) ;
	save_rooms();
	location.reload();
	
}

function remove_room() {
	var room = {} ;
	
	room.name = $("#room-name").val() ;
	if(!room.name)
		return ;
	room.name = room.name.trim();
	var index = -1 ;
	for( var i=0; i < rooms.length; i++) {
		// update existing room
		if( rooms[i].name == room.name ) {
			index = i ;
			break ;
			return ;
		}
	}
	
	if(index != -1) {
		var new_rooms = [] ;
		for(var j=0; j < rooms.length; j++) {
			if(j!=index)
				new_rooms.push(rooms[j]) ;
		}

		rooms = new_rooms ;		
		save_rooms();
		location.reload();
	}
}

function save_rooms() {
	var txt = [] ;
	for( var i=0; i < rooms.length; i++) {
		var room = rooms[i] ;
		//if( i>0)
		//	txt = txt + "\n" ;
		txt.push( room.name + "," + room.calendar_id + "," + room.openhab_id + "," + room.ref_temp + "," + room.target );
	}
	

	$
	.ajax({
		url : FILES_MANAGER_SERVICE_URL + '?filename=' + rooms_file_relative_path + '&data=' + JSON.stringify(txt) ,
		type : "GET",
		contentType : "application/json;charset=UTF-8",				
		crossDomain : true,
		success : function(weatherStations) {			
			alert("Data saved")		
		},
		error : function(jqXHR, exception) {
			alert("Error saving data")
		}
	});
	
	
}

function clear_form() {
	$("#room-name").val("") ;
	$("#room-calendar-id").val("") ;
	$("#room-openhab-id").val("") ;
	$("#room-ref-temp").val("") ;
	document.getElementById("room-target").selectedIndex = 0;
}

function load_curve_heating(room) {
	var curve = {} ;
	var curve_params = {} ;
	var curve_data = [] ;
	curve['curve_params'] = curve_params ;
	curve['curve_data'] = curve_data ;
	room['curve'] = curve ;
	
	
	
	// Load params
	$.get(heating_curves_file, function(data) {
		
		var lines = data.split("\n");

        $.each(lines, function(n, elem) {
		
		//nafarroa103,-25.444466405607827,-0.001733361231821985,26.381492696451637
        	var items = elem.split(",") ;
        	
        	if(items && items.length >=4) {
        		items[0] = items[0].trim();
        		if(items[0] == room.openhab_id) {
        			curve_params.a = parseFloat(items[1]) ;
        			curve_params.b = parseFloat(items[2]) ;
        			curve_params.c = parseFloat(items[3]) ;
        		}
        	}
        	
        });	
        
     // Load data
    	$.get(heating_curve_file + room.openhab_id, function(data2) {
    		var lines = data2.split("\n");
            $.each(lines, function(n, elem) {
            	var items = elem.split(",") ;
            	if(items && items.length >= 2 ) {
            		curve_data.push([parseInt(items[0]), parseFloat(items[1])]) ;
            	}
            });
            
            if(!graphShown) {
            	load_graph(room);            
            	graphShown = true ;
        	}
            
    	});
    	
    	
	}) ;
	
	
	
	
}

function load_graph(room) {
	
	if( room.curve && room.curve.curve_params && room.curve.curve_data && room.curve.curve_data.length>0 && 
			room.curve.curve_params.a && room.curve.curve_params.b && room.curve.curve_params.c) {
	$("#flot-placeholder").show();
	var points = [] ;
	var curve = [] ;
	
	for( var index=0; index < room.curve.curve_data.length; index++ ) {
		points.push( [ (room.curve.curve_data[index][0] - room.curve.curve_data[0][0]) / 60000, room.curve.curve_data[index][1]  ] ) ;
	}
	for( var i=0; i < num_points_in_curve; i++ ) {
		var time = (i * 60000) ; //+ room.curve.curve_data[0][0] ; 
		var temp = getTemp(room, time) ;
		curve.push([ i, temp ]);
	}
	
	var graph =  Flotr.draw(
			document.getElementById("flot-placeholder"), [		                
		                { data : points, label : 'y = Muestras', points : { show : true } },  // Scatter
		                { data : curve, label : 'y = Curva de regresión' }  // Regression
		              ],
		              {
		                legend : { position : 'se', backgroundColor : '#D2E8FF' },
		                title : 'Curva de Calentamiento de ' +  room.name
		              }
		            );
	}
	else {
		$("#flot-placeholder").hide();
	}
}

function getTemp(room, time) {
	return room.curve.curve_params.a*Math.exp(room.curve.curve_params.b * time) + room.curve.curve_params.c ;
}




