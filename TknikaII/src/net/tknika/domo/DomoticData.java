package net.tknika.domo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DomoticData implements Serializable {

    private static final long serialVersionUID = 672206565223771854L;

    private static final String DateHourOfDay = "dateHourOfDay";
    private static final String DateDayOfWeek = "dateDayOfWeek";
    private static final String DateDayOfMonth = "dateDayOfMonth";
    private static final String DateMonth = "dateMonth";
    private static final String Temperature = "temperature";
    private static final String ExternalTemperature = "externalTemperature";
    private static final String Presence = "presence";

    /*
     * private Date datetime; private Double temperature; private Double externalTemperature; private Boolean presence;
     */

    private Map<String, Serializable> variables = new HashMap<String, Serializable>();

    public DomoticData(Long datetime) {

        Calendar cal = GregorianCalendar.getInstance();
        cal.setTimeInMillis(datetime);

        variables.put(DateHourOfDay, cal.get(Calendar.HOUR_OF_DAY));
        variables.put(DateDayOfWeek, cal.get(Calendar.DAY_OF_WEEK));
        variables.put(DateDayOfMonth, cal.get(Calendar.DAY_OF_MONTH));
        variables.put(DateMonth, cal.get(Calendar.MONTH) + 1);
    }

    // public Long getDatetime() {
    // return (Long) variables.get(DateTime);
    // }

    public Double getTemperature() {
        return (Double) variables.get(Temperature);
    }

    public void setTemperature(Double temperature) {
        variables.put(Temperature, temperature);
    }

    public Double getExternalTemperature() {
        return (Double) variables.get(ExternalTemperature);
    }

    public void setExternalTemperature(Double externalTemperature) {
        variables.put(ExternalTemperature, externalTemperature);
    }

    public void setPresence(double presence) {
        variables.put(Presence, !(presence == 0.0));
    }

    public final Boolean getPresence() {
        return (Double) variables.get(Presence) != 0.0;
    }

    public final void setPresence(Boolean presence) {
        variables.put(Presence, presence ? 1.0 : 0.0);
    }

    public final Map<String, Serializable> getVariables() {
        return variables;
    }

    public final void setVariables(Map<String, Serializable> variables) {
        this.variables = variables;
    }

    public List<String> getVariableNames() {
        List<String> names = new ArrayList<String>();
        names.addAll(variables.keySet());
        Collections.sort(names);
        return names;
    }

    public List<Object> getVariableValues() {
        List<Object> values = new ArrayList<Object>();

        for (String col : getVariableNames()) {
            values.add(variables.get(col));
        }

        return values;
    }

    public List<Object> getVariableValues(Iterator<String> iterator) {

        if (iterator == null)
            return null;

        List<Object> values = new ArrayList<Object>();
        while (iterator.hasNext()) {
            String index = iterator.next();
            if (index == null) {
                values.add(null);
            } else {
                values.add(variables.get(index));
            }
        }

        return values;
    }

}
