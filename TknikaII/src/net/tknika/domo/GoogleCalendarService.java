package net.tknika.domo;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

public class GoogleCalendarService implements ReservationService {

    private static final Logger logger = Logger.getLogger(GoogleCalendarService.class);

    private static GoogleCalendarService instance;

    private String appName;
    private String accountID;
    private String privateKey;

    private com.google.api.services.calendar.Calendar service;

    public static GoogleCalendarService getInstance() {
        if (instance == null)
            instance = new GoogleCalendarService();
        return instance;
    }

    public static void main(String[] params) {
        GoogleCalendarService s = new GoogleCalendarService();
        s.getReservations();
    }

    public GoogleCalendarService() {

        appName = Config.getAppName();
        accountID = Config.getAccountID();
        privateKey = Config.getPrivateKey();

        auth();

    }

    private void auth() {
        try {
            File privateKeyFile = new File(privateKey);

            if (!privateKeyFile.exists())
                privateKeyFile = new File("META-INF/" + privateKey);

            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
                    .setJsonFactory(jsonFactory)
                    .setServiceAccountId(accountID)
                    .setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR))
                    .setServiceAccountPrivateKeyFromP12File(privateKeyFile)
                    .build();
            service = new com.google.api.services.calendar.Calendar.Builder(
                    httpTransport, jsonFactory, credential)
                    .setApplicationName(appName)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("Unable to initialize Google calendar service", e);
        }
    }

    private List<Reservation> addReservation(List<Reservation> reservations, Event event, Room room) {
        Reservation reservation = new Reservation();
        reservation.setDescription(event.getSummary());

        if (room == null)
            return reservations;

        reservation.setId(event.getId());
        reservation.setRoom(room);
        reservation.setStart(getDate(event.getStart()));
        reservation.setEnd(getDate(event.getEnd()));
        reservations.add(reservation);

        return reservations;
    }

    private Date getDate(EventDateTime eventDateTime) {
        if (eventDateTime.getDate() != null) {
            // all day event
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(eventDateTime.getDate().getValue());

            return cal.getTime();
        } else {
            return new Date(eventDateTime.getDateTime().getValue());
        }
    }

    @Override
    public Map<Room, List<Reservation>> getReservations() {
        return getReservations(null);
    }

    @Override
    public Map<Room, List<Reservation>> getReservations(ReservationFilter filter) {

        DateTime startDateTime = new DateTime(filter.getFrom());
        DateTime endDateTime = new DateTime(filter.getTo());

        Map<Room, List<Reservation>> res = new HashMap<Room, List<Reservation>>();

        // Is instantiated each time the method is called to allow hot changes
        RoomsManager roomsManager = new RoomsManager();
        Collection<Room> rooms = roomsManager.getRooms();

        for (Room room : rooms) {

            List<Reservation> reservations = new ArrayList<Reservation>();
            res.put(room, reservations);
            try {

                // Events events = service.events().list(room.getCalendarId()).execute();
                Events events = service.events().list(room.getCalendarId()).setTimeMin(startDateTime)
                        .setTimeMax(endDateTime).execute();

                if (events != null) {
                    List<Event> items = events.getItems();
                    if ((items != null) && !items.isEmpty()) {
                        for (Event event : items) {

                            if (filter != null) {
                                if (!filter.reject(event)) {
                                    reservations = addReservation(reservations, event, room);
                                }
                            } else {
                                reservations = addReservation(reservations, event, room);
                            }
                        }
                    }
                }

                logger.info("Room " + room.getName() + ": " + reservations.size() + " reservations (with filter)");

            } catch (Exception e) {
                logger.error("Exception getting all reservations from Google calendar service: " + room.getName(), e);
                return null;
            }
        }

        return res;
    }
}
