package net.tknika.domo;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import net.tknika.domo.ai.HeatingForecastModel;
import net.tknika.domo.ai.HeatingScheduling;
import net.tknika.domo.ai.ScheduledHeating;

import org.apache.log4j.Logger;

public class MainTask extends TimerTask {

    private static final Logger logger = Logger.getLogger(MainTask.class);

    private static final int MsInMinute = 60000;

    public static void main(String[] args) {
        new MainTask().run();
    }

    @Override
    public void run() {

        // Reload system configuration (to allow hot changes)
        Config.init();

        logger.info("--------- Executing main task ------------");

        ReservationService reservationService = GoogleCalendarService.getInstance();
        OpenhabService deviceService = new OpenhabService();

        RoomHeatingCurvesManager rhcm = new RoomHeatingCurvesManager();

        Calendar now = GregorianCalendar.getInstance();

        HeatingScheduling heatingScheduling = HeatingScheduling.getInstance();
        // HeatingScheduling size watcher
        if (now.get(Calendar.HOUR_OF_DAY) < 1
                && heatingScheduling.getScheduling().size() > HeatingScheduling.MaxSchedulings)
            heatingScheduling.getScheduling().clear();

        ReservationFilter filter = new ReservationFilter();

        Calendar startDay = GregorianCalendar.getInstance();
        Util.timeFieldsToMinimum(startDay);
        Calendar endDay = GregorianCalendar.getInstance();
        Util.timeFieldsToMaximum(endDay);

        filter.setFrom(startDay.getTime());
        filter.setTo(endDay.getTime());
        Map<Room, List<Reservation>> reservationsByRoom = reservationService.getReservations(filter);

        boolean isCentralHeatingOn = deviceService.isCentralHeatingOn();
        if (!isCentralHeatingOn)
            logger.info("Central Heating Off");

        logger.info("Checking reservations");
        if (reservationsByRoom != null) {
            for (Room room : reservationsByRoom.keySet()) {
                List<Reservation> reservations = reservationsByRoom.get(room);
                if (reservations == null || reservations.isEmpty()) {
                    if (room.manageElectricity())
                        deviceService.checkRoomElectricityState(room, ItemState.OFF);
                    if (room.manageHeating())
                        deviceService.checkRoomHeatingState(room, ItemState.OFF);
                    continue;
                }

                // Electrical treatment
                if (room.manageElectricity()) {
                    if (isBeingUsed(room, reservations, now.getTime()))
                        deviceService.checkRoomElectricityState(room, ItemState.ON);
                    else
                        deviceService.checkRoomElectricityState(room, ItemState.OFF);
                }

                // Heating treatment
                if (isCentralHeatingOn && room.manageHeating()) {

                    Reservation nextReservation = getNextReservation(reservations, now.getTimeInMillis());

                    if (nextReservation != null) {

                        ScheduledHeating schedule = heatingScheduling.getScheduling().get(nextReservation.getId());

                        HeatingForecastModel model = null;

                        // Is heating already scheduled?
                        if (schedule == null) {
                            // Schedule
                            schedule = new ScheduledHeating();
                            schedule.setId(nextReservation.getId());

                            model = HeatingForecastModel.load(room.getCode());

                            if (model == null) {

                                long start = Math.max(now.getTimeInMillis(), nextReservation.getStart().getTime()
                                        - Config.getMaxHeatingWindowTime());

                                schedule.setStart(start);
                                schedule.setEnd(nextReservation.getEnd().getTime());
                                logger.info("Heating scheduled in room: " + room.getName() + " without model. Start:  "
                                        +
                                        new Date(schedule.getStart()) + " Length: "
                                        + ((nextReservation.getStart().getTime() - start) / 60000) + "m");
                            } else {

                                DomoticData data = deviceService.getRoomData(room);
                                schedule.setDomoticDataAtStart(data);
                                Long anticipation = model.forecast(schedule, room.getCode());

                                anticipation = checkAnticipation(anticipation);

                                long start = Math.max(now.getTimeInMillis(), nextReservation.getStart().getTime()
                                        - anticipation);
                                schedule.setStart(start);
                                schedule.setEnd(nextReservation.getEnd().getTime());
                                logger.info("Heating scheduled in room: " + room.getName() + " with model. Start:  "
                                        +
                                        new Date(schedule.getStart()) + " Length: "
                                        + (anticipation / 60000) + "m");
                            }

                            heatingScheduling.getScheduling().put(schedule.getId(), schedule);

                        }
                        // Into scheduling window?
                        if (schedule.inHeatingWindowTime(now.getTimeInMillis())) {

                            DomoticData data = deviceService.getRoomData(room);

                            if (schedule.getDomoticDataAtStart() == null)
                                schedule.setDomoticDataAtStart(data);

                            Double temperature = data.getTemperature();
                            if (temperature != null && temperature < room.getTemperatureSetPoint()) {
                                deviceService.checkRoomHeatingState(room, ItemState.ON);
                            } else {
                                deviceService.checkRoomHeatingState(room, ItemState.OFF);
                                if (!schedule.isFinished()) {
                                    schedule.setFinished(true);
                                    long anticipationNeeded = now.getTimeInMillis() - schedule.getStart();
                                    schedule.setAnticipation(anticipationNeeded);
                                    if (anticipationNeeded > Config.getMinAnticipationNeeded()) {
                                        logger.info("Room " + room.getName() + " in comfort temperature. Time needed: "
                                                +
                                                ((anticipationNeeded) / 60000) + "m");
                                        try {
                                            HeatingForecastModel.addTrainingCase(schedule, room.getCode());
                                        } catch (IOException e) {
                                            logger.error(e.getMessage(), e);
                                        }
                                    } else {
                                        logger.info("Room " + room.getName()
                                                + " in comfort temperature. Time needed too short: " +
                                                ((anticipationNeeded) / 60000) + "m");
                                    }
                                }

                            }

                        }

                        heatingScheduling.save();
                    }

                    /*
                     * RoomHeatingCurve rhc = rhcm.getRoomHeatingCurve(room.getCode()); if (rhc == null) {
                     * logger.info("Room Heating Curve Not Created: " + room.getName()); Reservation.sort(reservations);
                     * 
                     * if (isTimeToBuildRoomHeatingCurve(now, reservations.get(0).getStart())) {
                     * RoomHeatingCurveBuilder.addSample(room, now); } } else {
                     * 
                     * DomoticData data = deviceService.getRoomData(room);
                     * 
                     * if (data != null) { if (isTimeToHeatRoom(room, now, reservations, data, rhc)) {
                     * deviceService.checkRoomHeatingState(room, ItemState.ON); } else {
                     * deviceService.checkRoomHeatingState(room, ItemState.OFF); } } }
                     */
                } else {
                    if (!room.manageHeating()) {
                        logger.info("Manage Heating off in Room " + room.getName());
                    }
                }

            }
        }

    }

    private Long checkAnticipation(Long anticipation) {
        if (anticipation == null || anticipation > Config.getMaxHeatingWindowTime())
            anticipation = Config.getMaxHeatingWindowTime();
        else if (anticipation < Config.getMinHeatingWindowTime())
            anticipation = Config.getMinHeatingWindowTime();

        return anticipation;
    }

    private Reservation getNextReservation(List<Reservation> reservations, long now) {

        long candidate = Long.MAX_VALUE;
        Reservation res = null;
        for (Reservation reservation : reservations) {
            if (reservation.getEnd().getTime() > now && reservation.getEnd().getTime() < candidate) {
                res = reservation;
                candidate = reservation.getEnd().getTime();
            }
        }

        return res;
    }

    private boolean isTimeToHeatRoom(Room room, Calendar now, List<Reservation> reservations, DomoticData data,
            RoomHeatingCurve rhc) {
        Double temperature = data.getTemperature();
        if (temperature == null)
            return false;

        if (temperature >= room.getTemperatureSetPoint())
            return false;

        // Room is cold...
        if (isBeingUsed(room, reservations, now.getTime()))
            return true;

        long nowInMillis = now.getTimeInMillis();
        for (Reservation reservation : reservations) {

            long anticipation = rhc.getAnticipationHeatingTime(reservation.getStart().getTime(), temperature,
                    room.getTemperatureSetPoint());

            if (anticipation > 0) {

                if (nowInMillis < reservation.getStart().getTime() && nowInMillis > anticipation)
                    return true;
            }

        }
        return false;
    }

    private boolean isTimeToBuildRoomHeatingCurve(Calendar now, Date reservationDate) {

        return now.getTimeInMillis() > (reservationDate.getTime() - Config.getFirstReservationHeatingAnticipation()
                * MsInMinute);

    }

    private boolean isBeingUsed(Room room, List<Reservation> reservations, Date now) {

        if (room == null || reservations == null || reservations.isEmpty())
            return false;

        for (Reservation reservation : reservations) {
            Date reservStart = new Date(reservation.getStart().getTime() - Config.getStartAllMinutesBefore()
                    * MsInMinute);
            Date reservEnd = new Date(reservation.getEnd().getTime() + Config.getFinishAllMinutesLater() * MsInMinute);
            if (now.after(reservStart) && now.before(reservEnd))
                return true;
        }

        return false;
    }

}
