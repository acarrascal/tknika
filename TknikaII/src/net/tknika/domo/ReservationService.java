package net.tknika.domo;

import java.util.List;
import java.util.Map;

public interface ReservationService {

    Map<Room, List<Reservation>> getReservations();

    Map<Room, List<Reservation>> getReservations(ReservationFilter filter);

}
