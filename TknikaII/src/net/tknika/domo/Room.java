package net.tknika.domo;

public class Room {

    public enum Target {
        electricity,
        heating,
        all
    }

    private String name;
    /**
     * Short name for references purposes
     */
    private String code;
    private String calendarId;
    /**
     * Comfort temperature of the room
     */
    private Double temperatureSetPoint;

    private Target target;

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final String getCode() {
        return code;
    }

    public final void setCode(String code) {
        this.code = code;
    }

    public final String getCalendarId() {
        return calendarId;
    }

    public final void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

    public Double getTemperatureSetPoint() {
        return temperatureSetPoint;
    }

    public void setTemperatureSetPoint(Double temperatureSetPoint) {
        this.temperatureSetPoint = temperatureSetPoint;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public boolean manageElectricity() {
        return target.equals(Target.all) || target.equals(Target.electricity);
    }

    public boolean manageHeating() {
        return target.equals(Target.all) || target.equals(Target.heating);
    }

}
