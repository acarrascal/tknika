package net.tknika.domo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public class RoomHeatingCurveBuilder {

    private static final Logger logger = Logger.getLogger(RoomHeatingCurveBuilder.class);

    private static final String fileNamePrefix = "data/heatingcurve_";

    /**
     * Check that the heating
     * 
     * @param room
     */
    public static void addSample(Room room, Calendar date) {

        // Is central heating on?
        OpenhabService deviceService = new OpenhabService();
        // if (!deviceService.isCentralHeatingOn()) // TODO
        // return;

        // Is heating on?
        deviceService.checkRoomHeatingState(room, ItemState.ON);

        // get sample
        DomoticData data = deviceService.getRoomData(room);
        Double temp = data.getTemperature();
        addSampleInFile(room, date, temp);
    }

    private static void addSampleInFile(Room room, Calendar date, Double temp) {

        if (temp == null)
            return;

        File file = new File(fileNamePrefix + room.getCode());

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.error(e.getMessage());
                return;
            }
        }

        List<Date> dates = new ArrayList<Date>();
        List<Double> temperatures = new ArrayList<Double>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                String[] fields = line.split(",");
                if (fields.length > 1) {

                    try {
                        dates.add(new Date(Long.parseLong(fields[0])));
                        temperatures.add(Double.parseDouble(fields[1]));
                    } catch (Exception e) {

                    }
                }

            }
            br.close();

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        // Append new sample if the temp has changed
        if (temp > (temperatures.isEmpty() ? 0 : temperatures.get(temperatures.size() - 1))) {

            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
                bw.append(date.getTimeInMillis() + "," + temp + "\n");
                logger.info(room.getName() + ": added heating curve point: " + Util.timeToString(date) + " , " + temp);
                bw.close();
                dates.add(date.getTime());
                temperatures.add(temp);

                if (dates.size() >= Config.getMinimumInstances()) {
                    RoomHeatingCurve rhc = RoomHeatingCurve.getInstance(dates, temperatures);
                    RoomHeatingCurvesManager rhcm = new RoomHeatingCurvesManager();
                    rhcm.addRoomHeatingCurve(room.getCode(), rhc);
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            logger.info(room.getName() + ":not added new point; same temp");
        }

    }
}
