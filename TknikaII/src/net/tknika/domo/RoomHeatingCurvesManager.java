package net.tknika.domo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public class RoomHeatingCurvesManager {

    private static final String fileName = "heatingcurves";
    private static final Logger logger = Logger.getLogger(RoomHeatingCurvesManager.class);

    private Map<String, RoomHeatingCurve> heatingCurvesByRoom = new HashMap<String, RoomHeatingCurve>();

    public RoomHeatingCurvesManager() {

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
            String line;

            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (!line.isEmpty()) {
                    String[] fields = line.split(",");
                    if (fields.length > 2) {

                        String roomId = fields[0].trim();
                        double a = Double.parseDouble(fields[1]);
                        double b = Double.parseDouble(fields[2]);
                        double c = Double.parseDouble(fields[3]);

                        RoomHeatingCurve rhc = new RoomHeatingCurve(a, b, c);

                        heatingCurvesByRoom.put(roomId, rhc);
                    }
                }
            }

            br.close();

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

    }

    public final RoomHeatingCurve getRoomHeatingCurve(String roomId) {
        return roomId != null ? heatingCurvesByRoom.get(roomId) : null;
    }

    public final void addRoomHeatingCurve(String roomId, RoomHeatingCurve rhc) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName), true));
            bw.append(roomId + "," + rhc.toString() + "\n");
            bw.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

}
