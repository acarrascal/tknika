package net.tknika.domo.ai;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.List;

import mltk.core.Instances;
import mltk.predictor.BaggedEnsemble;
import mltk.predictor.BaggedEnsembleLearner;
import mltk.predictor.tree.RegressionTreeLearner.Mode;
import mltk.predictor.tree.ensemble.rf.RandomRegressionTreeLearner;
import net.tknika.domo.Config;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

public class HeatingForecastModel {

    private static final Logger logger = Logger.getLogger(HeatingForecastModel.class);

    private final static String model_filename_prefix = "data/rf_model_";
    private final static String data_filename_prefix = "data/rf_data_";
    private final static String dataTest_filename_prefix = "data/rf_data_test_";
    private final static String Separator = ",";

    private BaggedEnsemble model;

    public static HeatingForecastModel load(String room) {

        try {

            HeatingForecastModel model = new HeatingForecastModel();

            BaggedEnsemble rf = new BaggedEnsemble();
            rf.read(new BufferedReader(new FileReader(model_filename_prefix + room)));

            model.setModel(rf);

            return model;
        } catch (Exception e) {
            if (!(e instanceof FileNotFoundException)) {
                logger.error(e.getMessage(), e);
            }
            return null;
        }

    }

    public Long forecast(ScheduledHeating schedule, String room) {

        try {

            createTestFile(schedule, room);
            int cols = getCSVcols(room);

            Instances instances = InstancesReader.read(dataTest_filename_prefix + room, cols - 1, Separator, true);
            double anticipation = model.regress(instances.get(0));
            return (long) anticipation;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public final BaggedEnsemble getModel() {
        return model;
    }

    public final void setModel(BaggedEnsemble model) {
        this.model = model;
    }

    /**
     * Add traininf case to CSV file. If the number of cases is enough the model will be created/re-trained
     * 
     * @param schedule
     * @param name
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void addTrainingCase(ScheduledHeating schedule, String room) throws IOException {
        Reader in;
        try {
            in = new FileReader(data_filename_prefix + room);
        } catch (FileNotFoundException e) {
            createTrainingFile(schedule, room);
            return;
        }

        CSVParser parser = new CSVParser(in, CSVFormat.DEFAULT);

        List<CSVRecord> records = parser.getRecords();

        // Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
        if (records == null || records.isEmpty()) {
            in.close();
            parser.close();
            createTrainingFile(schedule, room);
            return;
        }

        CSVRecord header = records.iterator().next();
        List<Object> values = schedule.getDomoticDataAtStart().getVariableValues(header.iterator());

        if (values == null) {
            in.close();
            parser.close();
            return;
        }

        values.set(values.size() - 1, schedule.getAnticipation());

        boolean train = false;

        if (records.size() > Config.getMinimumInstances()) {
            train = true;
        }
        //
        // while (records.iterator().hasNext()) {
        // records.iterator().next();
        // size++;
        // if (size > Config.getMinimumInstances()) {
        // train = true;
        // break;
        // }
        // }

        in.close();
        parser.close();

        FileWriter fw = new FileWriter(data_filename_prefix + room, true);

        CSVPrinter csvFilePrinter = new CSVPrinter(fw, CSVFormat.DEFAULT);
        csvFilePrinter.printRecord(values);

        fw.flush();
        fw.close();
        csvFilePrinter.close();

        if (train)
            trainModel(room);

    }

    private static void trainModel(String room) {
        try {

            int cols = getCSVcols(room);

            Instances instances = InstancesReader.read(data_filename_prefix + room, cols - 1, Separator, true);
            RandomRegressionTreeLearner rrtl = new RandomRegressionTreeLearner();
            rrtl.setMaxDepth(Config.getMaxDepth());
            rrtl.setConstructionMode(Mode.DEPTH_LIMITED);

            BaggedEnsembleLearner rfLearner = new BaggedEnsembleLearner(Config.getNumTrees(), rrtl);

            logger.info("RF model built: " + room);

            BaggedEnsemble rf = rfLearner.build(instances);
            PrintWriter pw = new PrintWriter(model_filename_prefix + room);
            rf.write(pw);
            pw.close();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static void createTrainingFile(ScheduledHeating schedule, String room) throws IOException {
        FileWriter fw = new FileWriter(data_filename_prefix + room);
        createCSVFile(schedule, room, fw);
    }

    private static void createTestFile(ScheduledHeating schedule, String room) throws IOException {
        FileWriter fw = new FileWriter(dataTest_filename_prefix + room);
        createCSVFile(schedule, room, fw);
    }

    private static void createCSVFile(ScheduledHeating schedule, String room, FileWriter fw) throws IOException {

        CSVPrinter csvFilePrinter = new CSVPrinter(fw, CSVFormat.DEFAULT);
        List<String> header = schedule.getDomoticDataAtStart().getVariableNames();
        header.add(ScheduledHeating.Anticipation);
        csvFilePrinter.printRecord(header);
        List<Object> values = schedule.getDomoticDataAtStart().getVariableValues();
        values.add(schedule.getAnticipation());
        csvFilePrinter.printRecord(values);

        fw.flush();
        fw.close();
        csvFilePrinter.close();

    }

    private static int getCSVcols(String room) throws IOException {
        Reader in = new FileReader(data_filename_prefix + room);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
        return records.iterator().next().size();
    }

}
