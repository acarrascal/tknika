package net.tknika.persistence.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import net.tknika.entities.Room;
import net.tknika.entities.ThermoCurve;
import net.tknika.entities.TrainingData;
import net.tknika.persistence.ThermodomoticsDAO;

public class JPAThermodomoticsDAO implements ThermodomoticsDAO {

    private EntityManagerFactory emf;
    private EntityManager em;

    private static JPAThermodomoticsDAO instance;

    public static JPAThermodomoticsDAO getInstance() {
        if (instance == null) {
            instance = new JPAThermodomoticsDAO();
        }
        return instance;
    }

    private JPAThermodomoticsDAO() {
        emf = Persistence.createEntityManagerFactory("thermoaulas");
        em = emf.createEntityManager();
    }

    @SuppressWarnings("unchecked")
    public List<Room> getRooms() {
        return em.createNamedQuery("Room.findAll").getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<TrainingData> getTrainingData(Room room) {
        return em.createNamedQuery("TrainingData.findAllForRoom").setParameter("room", room).getResultList();
    }

    public ThermoCurve getCurve(Room room) {
        return em.find(ThermoCurve.class, room.getId());
    }

    public Room getRoom(String roomId) {
        return em.find(Room.class, roomId);
    }

}
