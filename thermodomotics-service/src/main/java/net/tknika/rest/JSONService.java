package net.tknika.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import net.tknika.entities.Room;
import net.tknika.entities.ThermoCurve;
import net.tknika.entities.TrainingData;
import net.tknika.persistence.impl.JPAThermodomoticsDAO;

@Path("rest")
public class JSONService {

    // private final static Logger logger = Logger.getLogger(JSONService.class);

    JPAThermodomoticsDAO dao = JPAThermodomoticsDAO.getInstance();

    @GET
    @Path("/rooms")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Room> test() {
        return dao.getRooms();
    }

    @GET
    @Path("/trainingData")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TrainingData> getTrainingData(@QueryParam("id") String id) {
        if (dao.getRoom(id) == null) {
            return null;
        } else {
            return dao.getTrainingData(dao.getRoom(id));
        }
    }

    @GET
    @Path("/curve")
    @Produces(MediaType.APPLICATION_JSON)
    public ThermoCurve getCurve(@QueryParam("id") String id) {
        if (dao.getRoom(id) == null) {
            return null;
        } else {
            return dao.getCurve(dao.getRoom(id));
        }
    }

}
