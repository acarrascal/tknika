package net.tknika.domo.cli;

import java.util.Date;

import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.service.thermo.ThermodomoticService;
import net.tknika.domo.service.thermo.task.MainTask;

import org.apache.deltaspike.cdise.api.CdiContainer;
import org.apache.deltaspike.cdise.api.CdiContainerLoader;
import org.apache.log4j.Logger;

public class CliLauncher {

    private static final Logger logger = Logger.getLogger(CliLauncher.class);

    private static final String THREAD_NAME = "thermo-cli-lanucher";
    private CdiContainer cdiContainer;
    private ThermodomoticService thermodomoticService;

    public static void main(String[] args) {
        Thread.currentThread().setName(THREAD_NAME);
        final CliLauncher cl = new CliLauncher();
        cl.init(args);
        cl.start();
        Runtime.getRuntime().addShutdownHook(new Thread("shutdown_hook") {
            public void run() {
                cl.stop();
                cl.destroy();
            }
        });
    }

    public void init(String[] args) {
        try {
            cdiContainer = CdiContainerLoader.getCdiContainer();
            cdiContainer.boot();
            // thermodomoticService = BeanProvider.getContextualReference(ThermodomoticService.class, false);
            thermodomoticService = ThermodomoticService.getInstance();
            logger.info("Thermoaulas initialized");
        } catch (Exception e) {
            System.err.println("!!! Error initializing service !!!");
            e.printStackTrace(System.err);
            logger.error(e);
            System.exit(1);
        }
    }

    public void start() {
        try {
            thermodomoticService.start();

            Date date = new Date();
            MainTask task = new MainTask(); // BeanProvider.getContextualReference(MainTask.class, false);

            task.setExecution(date);
            task.setPriority(ExecutionPriority.NORMAL);
            thermodomoticService.execute(task);

            logger.info("Thermoaulas started");
        } catch (Exception e) {
            System.err.println("!!! Error starting !!!");
            e.printStackTrace(System.err);
            logger.error(e);
            System.exit(1);
        }
    }

    public void stop() {
        try {
            thermodomoticService.stop();
            logger.info("Thermoaulas stopped");
        } catch (Exception e) {
            System.err.println("!!! Error stopping service !!!");
            e.printStackTrace(System.err);
            logger.error(e);
            System.exit(1);
        }
    }

    public void destroy() {
        try {
            cdiContainer.shutdown();
            logger.info("destroy thermoaulas");
        } catch (Exception e) {
            System.err.println("Error destroying CLI launcher");
            e.printStackTrace(System.err);
            logger.error(e);
            System.exit(1);
        }
    }

}
