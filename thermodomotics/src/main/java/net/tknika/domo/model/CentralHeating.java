package net.tknika.domo.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class CentralHeating {

    private static final String configFile = "centralheating";
    private static final Logger logger = Logger.getLogger(CentralHeating.class);
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static boolean isOn(Double tempExt) {
        CentralHeatingData chd = readConfigFile();

        if (chd == null)
            logger.warn("No central heating config file");
        else {
            if (tempExt != null && chd.tempLimit != null) {
                if (tempExt > chd.tempLimit)
                    return false; // Temperature outside is over the limit
            }

            boolean specialDay = false;
            Calendar today = GregorianCalendar.getInstance();

            // Too late?
            if (chd.normal_end_hour != null && chd.normal_end_minute != null) {
                if (today.get(Calendar.HOUR) > chd.normal_end_hour
                        && today.get(Calendar.MINUTE) > chd.normal_end_minute)
                    return false;
            }

            if (today.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
                specialDay = true;
            else {
                for (Calendar cal : chd.days) {
                    if (cal.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) &&
                            cal.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
                        specialDay = true;
                        break;
                    }
                }
            }

            // Too early?
            if (specialDay) {
                if (chd.special_start_hour != null && chd.special_start_minute != null) {
                    if (today.get(Calendar.HOUR) < chd.special_start_hour
                            && today.get(Calendar.MINUTE) > chd.special_start_minute)
                        return false;
                }
            } else {
                if (chd.normal_start_hour != null && chd.normal_start_minute != null) {
                    if (today.get(Calendar.HOUR) < chd.normal_start_hour
                            && today.get(Calendar.MINUTE) > chd.normal_start_minute)
                        return false;
                }

            }

        }

        return true;
    }

    private static CentralHeatingData readConfigFile() {
        CentralHeatingData chd = new CentralHeatingData();
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(configFile)));
            String line;
            int lineCount = 0;

            while ((line = br.readLine()) != null) {
                try {
                    if (lineCount == 0) {
                        chd.tempLimit = Double.parseDouble(line);
                        lineCount++;
                    } else if (lineCount == 1) {
                        String[] tokens = line.split(":");
                        chd.normal_start_hour = Integer.parseInt(tokens[0]);
                        chd.normal_start_minute = Integer.parseInt(tokens[1]);
                        lineCount++;
                    } else if (lineCount == 2) {
                        String[] tokens = line.split(":");
                        chd.special_start_hour = Integer.parseInt(tokens[0]);
                        chd.special_start_minute = Integer.parseInt(tokens[1]);
                        lineCount++;
                    } else if (lineCount == 3) {
                        String[] tokens = line.split(":");
                        chd.normal_end_hour = Integer.parseInt(tokens[0]);
                        chd.normal_end_minute = Integer.parseInt(tokens[1]);
                        lineCount++;
                    } else {
                        // Special Days
                        Date date = sdf.parse(line);
                        Calendar cal = GregorianCalendar.getInstance();
                        cal.setTime(date);
                        chd.days.add(cal);
                    }

                } catch (Exception e) {
                }

            }

            br.close();

        } catch (FileNotFoundException e) {
            logger.error(e);
            return null;
        } catch (IOException e) {
            logger.error(e);
            return null;
        }

        return chd;
    }
}
