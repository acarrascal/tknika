package net.tknika.domo.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CentralHeatingData {
    public Double tempLimit = null;
    public List<Calendar> days = new ArrayList<Calendar>();
    public Integer normal_start_hour;
    public Integer normal_start_minute;
    public Integer normal_end_hour;
    public Integer normal_end_minute;
    public Integer special_start_hour;
    public Integer special_start_minute;

}
