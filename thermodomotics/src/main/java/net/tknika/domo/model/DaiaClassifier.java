package net.tknika.domo.model;

import java.io.IOException;
import java.util.Properties;

import net.tknika.domo.Config;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.persistence.impl.JPAThermodomoticsDAO;
import net.tknika.domo.service.classifier.ClassifierService;
import net.tknika.domo.service.classifier.curve.ThermoCurveServiceImpl;
import net.tknika.domo.service.classifier.rtree.RandomTreeServiceImpl;

import org.apache.log4j.Logger;

public class DaiaClassifier {

    private static final Logger logger = Logger.getLogger(DaiaClassifier.class);

    private Reservation reservation;
    private int minimumInstances;

    private ThermodomoticsDAO dao;

    public DaiaClassifier() {
        dao = JPAThermodomoticsDAO.getInstance();
        Properties p = new Properties();

        try {
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            try {
                minimumInstances = Integer.parseInt(p.getProperty("domotic.minimumInstances"));
            } catch (Exception e) {
            }

        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public DaiaClassifier(Reservation reservation) {
        this();
        this.reservation = reservation;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public long getTimeToHeatOn(double initTemp, double finalTemp, double externalTemp) {
        try {
            ClassifierService classifier;
            if (reservation.getRoom() != null
                    && dao.getTrainingData(reservation.getRoom()) != null
                    && !dao.getTrainingData(reservation.getRoom()).isEmpty()
                    && dao.getTrainingData(reservation.getRoom()).size() >= minimumInstances) {
                RandomTreeServiceImpl rtClassifier = new RandomTreeServiceImpl(); // BeanProvider.getContextualReference(RandomTreeServiceImpl.class,false);
                rtClassifier.setRoom(reservation.getRoom());
                classifier = rtClassifier;
            } else {
                ThermoCurveServiceImpl tcClassifier = new ThermoCurveServiceImpl(); // BeanProvider.getContextualReference(ThermoCurveServiceImpl.class,false);
                tcClassifier.setReservation(reservation);
                classifier = tcClassifier;
            }
            return classifier.getTimeToHeatOn(initTemp, finalTemp, externalTemp);
        } catch (Exception e) {
            logger.error("error getting time to heat on", e);
            return 0;
        }
    }

    public ThermodomoticsDAO getDao() {
        return dao;
    }

    public void setDao(ThermodomoticsDAO dao) {
        this.dao = dao;
    }

}
