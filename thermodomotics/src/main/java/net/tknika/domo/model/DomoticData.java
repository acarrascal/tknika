package net.tknika.domo.model;

import java.util.Date;

public class DomoticData {

    private Date datetime;
    private Double temperature;
    private Double externalTemperature;
    private Boolean presence;

    public DomoticData(Date datetime) {
        this.datetime = datetime;
    }

    public Date getDatetime() {
        return datetime;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getExternalTemperature() {
        return externalTemperature;
    }

    public void setExternalTemperature(Double externalTemperature) {
        this.externalTemperature = externalTemperature;
    }

    public void setPresence(double presence) {
        this.presence = !(presence == 0.0);
    }

    public final Boolean getPresence() {
        return presence;
    }

    public final void setPresence(Boolean presence) {
        this.presence = presence;
    }

}
