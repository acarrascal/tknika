package net.tknika.domo.model;

public enum ExecutionPriority {

    REAL_TIME,
    VERY_HIGH,
    HIGH,
    NORMAL,
    LOW,
    VERY_LOW

}
