package net.tknika.domo.model;

import java.util.Date;

public class MeteoData {

    private Date datetime;
    private Double temperature;
    private Double humidity;
    private Double radiation;

    public MeteoData(Date datetime) {
        super();
        this.datetime = datetime;
    }

    public Date getDatetime() {
        return datetime;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getRadiation() {
        return radiation;
    }

    public void setRadiation(Double radiation) {
        this.radiation = radiation;
    }

}
