package net.tknika.domo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity implementation class for Entity: ReservationDates
 * 
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "ReservationDates.find", query = "select r from ReservationDates r"),
        @NamedQuery(name = "ReservationDates.deleteAll", query = "delete from ReservationDates")
})
public class ReservationDates implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    @ElementCollection
    @Temporal(TemporalType.TIMESTAMP)
    List<Date> dates;

    private static final long serialVersionUID = 1L;

    public ReservationDates() {
        dates = new ArrayList<Date>();
    }

    public void addDates(Reservation rsv) {
        dates.add(rsv.getStart());
        dates.add(rsv.getEnd());
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean equals(List<Date> otherDates) {
        if (otherDates == null)
            return false;
        Collections.sort(otherDates);
        Collections.sort(dates);
        if (otherDates.size() != dates.size())
            return false;
        for (int i = 0; i < dates.size(); i++) {
            if (otherDates.get(i).getTime() != dates.get(i).getTime()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return dates.toString();
    }

}
