package net.tknika.domo.model;

import java.util.Date;

import com.google.api.services.calendar.model.Event;

public class ReservationFilter {

    private String roomId;
    private Date from;
    private Date to;
    private boolean firstOfDay;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public boolean isFirstOfDay() {
        return firstOfDay;
    }

    public void setFirstOfDay(boolean firstOfDay) {
        this.firstOfDay = firstOfDay;
    }

    public boolean reject(Event event) {
        Date eventStart;
        Date eventEnd;
        if (event.getStart().getDate() != null) {
            eventStart = new Date(event.getStart().getDate().getValue());
        } else {
            eventStart = new Date(event.getStart().getDateTime().getValue());
        }
        if (event.getEnd().getDate() != null) {
            eventEnd = new Date(event.getEnd().getDate().getValue());
        } else {
            eventEnd = new Date(event.getEnd().getDateTime().getValue());
        }

        return eventEnd.before(getFrom()) || eventStart.after(getTo());

    }

}
