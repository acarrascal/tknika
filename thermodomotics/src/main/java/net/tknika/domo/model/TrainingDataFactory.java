package net.tknika.domo.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class TrainingDataFactory {

    private final static double EXTERNAL_TEMP = 5;
    private final static double TEMP_VARIATION = 5;

    /**
     * 
     * @param Teq
     *            equilibrium temperature
     * @param Tini
     *            initial temperature
     * @param hs
     *            heating slope
     * @param Tf
     *            Comfort temperature
     * @param ATf
     *            degrees allowed below final temperature
     * @param size
     *            number of samples
     * @return training data
     */
    public static List<TrainingData> generateTrainingData(double Teq, double Tini, double hs, double Tf, double ATf,
            int size) {
        if (size <= 0)
            return null;

        List<TrainingData> res = new ArrayList<TrainingData>(size);
        RoomHeatingCurve rhc = new RoomHeatingCurve(Tini - Teq, hs, Teq);

        Date date = new Date();

        for (int i = 0; i < size; i++) {
            double T0 = Math.random() * (Tf - Tini - ATf);
            double Tfinal = Tf - ATf * Math.random();

            long sDate = rhc.getAnticipationHeatingTime(date.getTime(), Tini + T0, Tfinal);
            TrainingData td = new TrainingData();
            td.setFinalTemperature(Tfinal);
            td.setInitialTemperature(Tini + T0);
            td.setStart(sDate);
            td.setEnd(date.getTime());
            td.setExternalTemperature(getExternalTemp());
            res.add(td);
        }
        return res;
    }

    public static double getExternalTemp() {
        return getRandomDouble(EXTERNAL_TEMP, TEMP_VARIATION);
    }

    public static double getRandomDouble(double reference, double variation) {
        Random r = new Random();
        double rangeMin = reference - variation;
        double rangeMax = reference + variation;
        return rangeMin + (rangeMax - rangeMin) * r.nextDouble();
    }

    public static void main(String args[]) {
        List<TrainingData> tdl = TrainingDataFactory.generateTrainingData(35.0, 0, -0.002, 21, 5, 1000);
        for (TrainingData td : tdl)
            System.out.println(td.toString());

    }
}
