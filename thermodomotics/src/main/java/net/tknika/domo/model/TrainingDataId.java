package net.tknika.domo.model;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TrainingDataId implements Serializable {

    /**
     * Generated serial version UID
     */
    private static final long serialVersionUID = -8234776839523190363L;

    private String room;
    private long start;

    public String getRoom() {
        return room;
    }

    public void setId(String room) {
        this.room = room;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        TrainingDataId other = (TrainingDataId) obj;
        if (other.getRoom().equals(this.room) && other.getStart() == this.start) {
            return true;
        } else {
            return false;
        }
    }

}
