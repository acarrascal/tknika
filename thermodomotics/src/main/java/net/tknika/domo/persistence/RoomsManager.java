package net.tknika.domo.persistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.tknika.domo.model.Room;

import org.apache.log4j.Logger;

public class RoomsManager {

    private static final String fileName = "rooms";
    private static final Logger logger = Logger.getLogger(RoomsManager.class);

    private Map<String, Room> rooms = new HashMap<String, Room>();

    public RoomsManager() {

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
            String line;

            while ((line = br.readLine()) != null) {
                String[] fields = line.split(",");
                if (fields.length > 2) {
                    Room r = new Room();
                    r.setName(fields[0].trim());
                    r.setCode(fields[1].trim());
                    r.setCalendarId(fields[2]);
                    rooms.put(r.getName().toLowerCase(), r);
                }
            }

            br.close();

        } catch (FileNotFoundException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        }

    }

    public final Room getRoom(String name) {
        return name != null ? rooms.get(name.trim().toLowerCase()) : null;
    }

    public final Collection<Room> getRooms() {
        return rooms.values();
    }

}
