package net.tknika.domo.persistence;

import java.util.Date;
import java.util.List;

import net.tknika.domo.model.ReservationDates;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.ThermoCurve;
import net.tknika.domo.model.TrainingData;

public interface ThermodomoticsDAO {

    // ROOMS

    List<Room> getRooms();

    List<Room> getRooms(String roomName);

    Room getRoom(String roomID);

    void save(Room room);

    void deleteRooms();

    void deleteRoom(String id);

    // TRAINING DATA

    TrainingData getTrainingData(Room room, Date datetime);

    List<TrainingData> getTrainingData(Room room, Date from, Date to);

    List<TrainingData> getTrainingData(Room room);

    void save(TrainingData trainingData);

    TrainingData update(TrainingData trainingData);

    void deleteTrainingDatas(Room room);

    void deleteTrainingDatas();

    // ThermoCurve

    ThermoCurve getCurve(Room room);

    void save(ThermoCurve curve);

    ThermoCurve update(ThermoCurve curve);

    void deleteCurves();

    // ReservationDates

    void save(ReservationDates rDates);

    ReservationDates getReservationDates();

    ReservationDates update(ReservationDates rDates);

    void deleteReservationDates();

}
