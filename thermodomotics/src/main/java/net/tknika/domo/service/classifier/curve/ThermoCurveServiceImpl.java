package net.tknika.domo.service.classifier.curve;

import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.RoomHeatingCurve;
import net.tknika.domo.model.ThermoCurve;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.persistence.impl.JPAThermodomoticsDAO;
import net.tknika.domo.service.classifier.ClassifierService;

public class ThermoCurveServiceImpl extends ClassifierService {

    ThermodomoticsDAO dao;

    private Reservation reservation;

    public ThermoCurveServiceImpl(Reservation reservation) {
        this();
        this.reservation = reservation;
    }

    public ThermoCurveServiceImpl() {
        dao = JPAThermodomoticsDAO.getInstance();
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    @Override
    public long getTimeToHeatOn(double initTemp, double finalTemp, double externalTemp) {
        ThermoCurve curve = dao.getCurve(reservation.getRoom());
        RoomHeatingCurve heatingCurve = RoomHeatingCurve.getInstance(curve.getDates(), curve.getTemperatures());
        return heatingCurve.getAnticipationHeatingTime(reservation.getStart().getTime(), initTemp, finalTemp);
    }

}
