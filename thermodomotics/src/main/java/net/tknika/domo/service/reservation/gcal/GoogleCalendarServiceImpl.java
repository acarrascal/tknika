package net.tknika.domo.service.reservation.gcal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import net.tknika.domo.Config;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.ReservationFilter;
import net.tknika.domo.model.Room;
import net.tknika.domo.persistence.RoomsManager;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.persistence.impl.JPAThermodomoticsDAO;
import net.tknika.domo.service.reservation.ReservationService;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;

public class GoogleCalendarServiceImpl implements ReservationService {

    // @Inject
    // @ConfigProperty(name = "gcal.nafarroaAretoa")
    // private String NAFARROA_ARETOA;
    // @Inject
    // @ConfigProperty(name = "gcal.nafarroaAretoa.key")
    // private String KEY_NAFARROA_ARETOA;

    private static final Logger logger = Logger.getLogger(GoogleCalendarServiceImpl.class);

    private ThermodomoticsDAO dao;

    private static GoogleCalendarServiceImpl instance;

    private int minimumHour;
    private int minimumMinute;
    private int maximumHour;
    private int maximumMinute;
    private String appName;
    private String accountID;
    private String privateKey;
    // private String __calendarId;
    private com.google.api.services.calendar.Calendar service;

    private RoomsManager roomsManager;

    public static GoogleCalendarServiceImpl getInstance() {
        if (instance == null)
            instance = new GoogleCalendarServiceImpl();
        return instance;
    }

    public static void main(String[] params) {
        GoogleCalendarServiceImpl s = new GoogleCalendarServiceImpl();
        s.getReservations();
    }

    public GoogleCalendarServiceImpl() {

        try {

            Properties p = new Properties();

            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            minimumHour = Integer.parseInt(p.getProperty("gcal.minimumHour"));
            maximumHour = Integer.parseInt(p.getProperty("gcal.maximumHour"));

            try {
                minimumMinute = Integer.parseInt(p.getProperty("gcal.minimumMinute"));
            } catch (Exception e) {
                minimumMinute = 0;
            }
            try {
                maximumMinute = Integer.parseInt(p.getProperty("gcal.maximumMinute"));
            } catch (Exception e) {
                maximumMinute = 0;
            }

            appName = p.getProperty("gcal.appname");
            accountID = p.getProperty("gcal.accountID");
            privateKey = p.getProperty("gcal.privateKey");
            // calendarId = p.getProperty("gcal.calendarID");
            // if (calendarId == null)
            // calendarId = "primary";

            dao = JPAThermodomoticsDAO.getInstance();

            // local
            // File privateKeyFile = new File(getClass().getClassLoader().getResource(privateKey).getFile());
            // server
            File privateKeyFile = new File(privateKey);

            if (!privateKeyFile.exists())
                privateKeyFile = new File("META-INF/" + privateKey);

            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
                    .setJsonFactory(jsonFactory)
                    .setServiceAccountId(accountID)
                    .setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR))
                    .setServiceAccountPrivateKeyFromP12File(privateKeyFile)
                    .build();
            service = new com.google.api.services.calendar.Calendar.Builder(
                    httpTransport, jsonFactory, credential)
                    .setApplicationName(appName)
                    .build();

            roomsManager = new RoomsManager();

        } catch (Exception e) {
            throw new RuntimeException("Unable to initialize Google calendar service", e);
        }
    }

    @Override
    public List<Reservation> getReservations() {

        Collection<Room> rooms = roomsManager.getRooms();
        List<Reservation> reservations = new ArrayList<Reservation>();

        for (Room room : rooms) {
            try {
                Events events = service.events().list(room.getCalendarId()).execute();
                List<Event> items = events.getItems();
                if ((items != null) && !items.isEmpty()) {

                    for (Event event : items) {
                        reservations = addReservation(reservations, event, room);
                    }
                }
            } catch (Exception e) {
                reservations = null;
                throw new RuntimeException("Exception getting all reservations from Google calendar service", e);
            }
        }
        logger.info(reservations.size() + " reservations ");
        return reservations;
    }

    private List<Reservation> addReservation(List<Reservation> reservations, Event event, Room room) {
        Reservation reservation = new Reservation();
        reservation.setDescription(event.getSummary());

        if (room == null)
            return reservations;
        // if (dao.getRooms(NAFARROA_ARETOA) != null && dao.getRooms(NAFARROA_ARETOA).size() > 0) {
        // room = dao.getRooms(NAFARROA_ARETOA).get(0);
        // } else {
        // room = new Room();
        // room.setName(NAFARROA_ARETOA);
        // room.setId(KEY_NAFARROA_ARETOA);
        // dao.save(room);
        // }
        if (DateUtils.isSameDay(getStartDate(event.getStart()), getEndDate(event.getEnd()))) {
            reservation.setRoom(room);
            reservation.setStart(getStartDate(event.getStart()));
            reservation.setEnd(getEndDate(event.getEnd()));
            reservations.add(reservation);
        } else {
            List<Reservation> cuttedReservations = cutReservation(getStartDate(event.getStart()),
                    getEndDate(event.getEnd()), getStartDate(event.getStart()), event, room);
            for (Reservation rsv : cuttedReservations) {
                reservations.add(rsv);
            }
        }
        return reservations;
    }

    // private final Room _getRoom(Event event) {
    //
    // String loc = event.getLocation();
    // if (loc != null) {
    // Room room = roomsManager.getRoom(loc);
    // if (room != null)
    // return room;
    // }
    //
    // List<EventAttendee> atts = event.getAttendees();
    //
    // if (atts != null) {
    // for (EventAttendee att : atts) {
    // Room room = roomsManager.getRoom(att.getDisplayName());
    // if (room != null)
    // return room;
    // }
    // }
    //
    // return null;
    // }

    private List<Reservation> cutReservation(Date start, Date end, Date day, Event event, Room room) {
        List<Reservation> reservations = new ArrayList<Reservation>();

        if (room != null) {

            while (day.before(end) || DateUtils.isSameDay(day, end)) {
                Reservation reservation = new Reservation();
                reservation.setDescription(event.getSummary());
                reservation.setRoom(room);
                if (DateUtils.isSameDay(day, start)) {
                    reservation.setStart(start);
                    reservation.setEnd(timeFieldsToMaximum(day));
                } else if (DateUtils.isSameDay(day, end)) {
                    reservation.setStart(timeFieldsToMinimum(day));
                    reservation.setEnd(end);
                } else {
                    reservation.setStart(timeFieldsToMinimum(day));
                    reservation.setEnd(timeFieldsToMaximum(day));

                }
                reservations.add(reservation);
                day = DateUtils.addDays(day, 1);
            }
        }
        return reservations;
    }

    private Date getStartDate(EventDateTime eventDateTime) {
        if (eventDateTime.getDate() != null) {
            // all day event
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(eventDateTime.getDate().getValue());
            timeFieldsToMinimum(cal);
            return cal.getTime();
        } else {
            return new Date(eventDateTime.getDateTime().getValue());
        }
    }

    private Date getEndDate(EventDateTime eventDateTime) {
        if (eventDateTime.getDate() != null) {
            // all day event
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(eventDateTime.getDate().getValue());
            timeFieldsToMaximum(cal);
            cal.add(Calendar.DAY_OF_YEAR, -1);
            return cal.getTime();
        } else {
            return new Date(eventDateTime.getDateTime().getValue());
        }
    }

    @Override
    public void deleteReservations() {
        // try {
        // Events events = service.events().list(calendarId).execute();
        // List<Event> items = events.getItems();
        // if ((items != null) && !items.isEmpty()) {
        // for (Event event : items) {
        // service.events().delete(calendarId, event.getId()).execute();
        // }
        // }
        // } catch (Exception e) {
        // throw new RuntimeException("Exception deleting all reservations from Google calendar service", e);
        // }
    }

    @Override
    public List<Reservation> getReservations(ReservationFilter filter) {

        List<Reservation> reservations = new ArrayList<Reservation>();
        Collection<Room> rooms = roomsManager.getRooms();

        for (Room room : rooms) {
            try {
                Events events = service.events().list(room.getCalendarId()).execute();

                if (events != null) {
                    List<Event> items = events.getItems();
                    if ((items != null) && !items.isEmpty()) {
                        for (Event event : items) {

                            // if (eventEnd.before(new Date())) {
                            // continue;
                            // }

                            if (!filter.reject(event)) {

                                // if ((eventStart.after(filter.getFrom()) || eventStart.equals(filter.getFrom())) &&
                                // (eventEnd.before(filter.getTo()) || eventEnd.equals(filter.getTo()))) {
                                reservations = addReservation(reservations, event, room);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                reservations = null;
                throw new RuntimeException("Exception getting all reservations from Google calendar service", e);
            }
        }
        logger.info(reservations.size() + " reservations (with filter)");
        return reservations;
    }

    @Override
    public Reservation getReservation(String roomId, long date) {
        Reservation reservation = null;

        Room room = dao.getRoom(roomId);
        if (room == null)
            return null;

        try {
            Events events = service.events().list(room.getCalendarId()).execute();

            List<Event> items = events.getItems();
            if ((items != null) && !items.isEmpty()) {
                return null;
            }
            for (Event event : items) {
                if (roomId.equals(event.getLocation()) && date == event.getStart().getDate().getValue()) {
                    reservation = new Reservation();

                    reservation.setRoom(room);
                    if (event.getStart().getDateTime() == null) {
                        reservation.setStart(new Date(event.getStart().getDate().getValue()));
                    } else {
                        reservation.setStart(new Date(event.getStart().getDateTime().getValue()));
                    }
                    if (event.getEnd().getDateTime() == null) {
                        reservation.setEnd(new Date(event.getEnd().getDate().getValue()));
                    } else {
                        reservation.setEnd(new Date(event.getEnd().getDateTime().getValue()));
                    }
                    reservation.setDescription(event.getSummary());

                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Exception getting a reservation from Google calendar service", e);
        }
        return reservation;
    }

    @Override
    public void makeReservation(Reservation reservation) {
        try {
            Event event = new Event();
            event.setSummary(reservation.getDescription());
            event.setLocation(reservation.getRoom().getCode());
            if (dao.getRoom(reservation.getRoom().getCode()) == null) {
                Room room = new Room();
                room.setCode(event.getLocation());
                room.setName(event.getLocation());
                dao.save(room);
            }
            DateTime start = new DateTime(reservation.getStart());
            event.setStart(new EventDateTime().setDateTime(start));
            DateTime end = new DateTime(reservation.getEnd());
            event.setEnd(new EventDateTime().setDateTime(end));
            event = service.events().insert(reservation.getRoom().getCalendarId(), event).execute();
        } catch (Exception e) {
            throw new RuntimeException("Exception making reservation to Google calendar service", e);
        }
    }

    @Override
    public void deleteReservation(String id) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<Reservation> getReservations(Room room) {
        List<Reservation> reservations = null;

        try {
            Events events = service.events().list(room.getCalendarId()).execute();
            List<Event> items = events.getItems();
            if ((items != null) && !items.isEmpty()) {
                if (reservations == null) {
                    reservations = new ArrayList<Reservation>();
                }

                for (Event event : items) {
                    reservations = addReservation(reservations, event, room);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Exception getting all reservations from Google calendar service", e);
        }
        logger.info(reservations.size() + " reservations ");
        return reservations;
    }

    /**
     * Sets date to minimum reservation time
     * 
     * @param date
     */
    private void timeFieldsToMinimum(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, minimumHour);
        date.set(Calendar.MINUTE, minimumMinute);
    }

    /**
     * Sets date to minimum reservation time
     * 
     * @param date
     */
    private Date timeFieldsToMinimum(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, minimumHour);
        cal.set(Calendar.MINUTE, minimumMinute);
        return cal.getTime();
    }

    /**
     * Sets date to maximum reservation time
     * 
     * @param date
     */
    private void timeFieldsToMaximum(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, maximumHour);
        date.set(Calendar.MINUTE, maximumMinute);
    }

    /**
     * Sets date to maximum reservation time
     * 
     * @param date
     */
    private Date timeFieldsToMaximum(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, maximumHour);
        cal.set(Calendar.MINUTE, maximumMinute);
        return cal.getTime();
    }

}
