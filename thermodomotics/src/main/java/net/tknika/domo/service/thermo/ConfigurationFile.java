package net.tknika.domo.service.thermo;

import net.tknika.domo.Config;

import org.apache.deltaspike.core.api.config.PropertyFileConfig;

public class ConfigurationFile implements PropertyFileConfig {

    private static final long serialVersionUID = -7567522904010471803L;
    public static final String CONFIGURATION_FILE = Config.File;

    @Override
    public String getPropertyFileName() {
        return CONFIGURATION_FILE;
    }

}
