package net.tknika.domo.service.thermo;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import net.tknika.domo.Config;

import org.apache.log4j.Logger;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.meta.Bagging;
import weka.classifiers.trees.REPTree;
import weka.core.AdditionalMeasureProducer;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Option;
import weka.core.OptionHandler;
import weka.core.PartitionGenerator;
import weka.core.Randomizable;
import weka.core.RevisionUtils;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformationHandler;
import weka.core.Utils;
import weka.core.WeightedInstancesHandler;

public class RandomForest
        extends AbstractClassifier
        implements OptionHandler, Randomizable, WeightedInstancesHandler, AdditionalMeasureProducer,
        TechnicalInformationHandler, PartitionGenerator
{
    static final long serialVersionUID = 1116839470751428698L;

    private static final Logger logger = Logger.getLogger(RandomForest.class);

    /* 114 */protected int m_numTrees;

    /* 118 */protected int m_numFeatures;

    /* 121 */protected int m_randomSeed = 1;

    /* 124 */protected int m_KValue = 0;

    /* 127 */protected Bagging m_bagger = null;

    /* 130 */protected int m_MaxDepth;

    /* 133 */protected int m_numExecutionSlots = 1;

    /* 136 */protected boolean m_printTrees = false;

    public RandomForest() {
        m_MaxDepth = 100;
        m_numFeatures = 3;
        m_numTrees = 1000;

        Properties p = new Properties();
        try {
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            try {
                m_numTrees = Integer.parseInt(p.getProperty("thermo.randomForest.numTrees"));
            } catch (Exception e) {
            }
            try {
                m_numFeatures = Integer.parseInt(p.getProperty("thermo.randomForest.numFeatures"));
            } catch (Exception e) {
            }
            try {
                m_MaxDepth = Integer.parseInt(p.getProperty("thermo.randomForest.maxDepth"));
            } catch (Exception e) {
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public String globalInfo()
    {
        /* 145 */return "Class for constructing a forest of random trees.\n\nFor more information see: \n\n"
                + getTechnicalInformation().toString();
    }

    public TechnicalInformation getTechnicalInformation()
    {
        /* 161 */TechnicalInformation result = new TechnicalInformation(TechnicalInformation.Type.ARTICLE);
        /* 162 */result.setValue(TechnicalInformation.Field.AUTHOR, "Leo Breiman");
        /* 163 */result.setValue(TechnicalInformation.Field.YEAR, "2001");
        /* 164 */result.setValue(TechnicalInformation.Field.TITLE, "Random Forests");
        /* 165 */result.setValue(TechnicalInformation.Field.JOURNAL, "Machine Learning");
        /* 166 */result.setValue(TechnicalInformation.Field.VOLUME, "45");
        /* 167 */result.setValue(TechnicalInformation.Field.NUMBER, "1");
        /* 168 */result.setValue(TechnicalInformation.Field.PAGES, "5-32");
        /* 170 */return result;
    }

    public String numTreesTipText()
    {
        /* 179 */return "The number of trees to be generated.";
    }

    public int getNumTrees()
    {
        /* 189 */return this.m_numTrees;
    }

    public void setNumTrees(int newNumTrees)
    {
        /* 199 */this.m_numTrees = newNumTrees;
    }

    public String numFeaturesTipText()
    {
        /* 208 */return "The number of attributes to be used in random selection (see RandomTree).";
    }

    public int getNumFeatures()
    {
        /* 218 */return this.m_numFeatures;
    }

    public void setNumFeatures(int newNumFeatures)
    {
        /* 228 */this.m_numFeatures = newNumFeatures;
    }

    public String seedTipText()
    {
        /* 237 */return "The random number seed to be used.";
    }

    public void setSeed(int seed)
    {
        /* 247 */this.m_randomSeed = seed;
    }

    public int getSeed()
    {
        /* 257 */return this.m_randomSeed;
    }

    public String maxDepthTipText()
    {
        /* 267 */return "The maximum depth of the trees, 0 for unlimited.";
    }

    public int getMaxDepth()
    {
        /* 276 */return this.m_MaxDepth;
    }

    public void setMaxDepth(int value)
    {
        /* 285 */this.m_MaxDepth = value;
    }

    public String printTreesTipText()
    {
        /* 295 */return "Print the individual trees in the output";
    }

    public void setPrintTrees(boolean print)
    {
        /* 304 */this.m_printTrees = print;
    }

    public boolean getPrintTrees()
    {
        /* 313 */return this.m_printTrees;
    }

    public double measureOutOfBagError()
    {
        /* 323 */if (this.m_bagger != null)
            /* 324 */return this.m_bagger.measureOutOfBagError();
        /* 325 */return 0.0 / 0.0;
    }

    public void setNumExecutionSlots(int numSlots)
    {
        /* 335 */this.m_numExecutionSlots = numSlots;
    }

    public int getNumExecutionSlots()
    {
        /* 345 */return this.m_numExecutionSlots;
    }

    public String numExecutionSlotsTipText()
    {
        /* 354 */return "The number of execution slots (threads) to use for constructing the ensemble.";
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Enumeration enumerateMeasures()
    {
        /* 365 */Vector newVector = new Vector(1);
        /* 366 */newVector.addElement("measureOutOfBagError");
        /* 367 */return newVector.elements();
    }

    public double getMeasure(String additionalMeasureName)
    {
        /* 379 */if (additionalMeasureName.equalsIgnoreCase("measureOutOfBagError")) {
            /* 380 */return measureOutOfBagError();
        }
        /* 382 */throw new IllegalArgumentException(additionalMeasureName + " not supported (RandomForest)");
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Enumeration listOptions()
    {
        /* 394 */Vector newVector = new Vector();

        /* 396 */newVector.addElement(new Option("\tNumber of trees to build.", "I", 1, "-I <number of trees>"));

        /* 400 */newVector.addElement(new Option("\tNumber of features to consider (<1=int(logM+1)).", "K", 1,
                "-K <number of features>"));

        /* 404 */newVector.addElement(new Option("\tSeed for random number generator.\n\t(default 1)", "S", 1, "-S"));

        /* 409 */newVector.addElement(new Option("\tThe maximum depth of the trees, 0 for unlimited.\n\t(default 0)",
                "depth", 1, "-depth <num>"));

        /* 414 */newVector.addElement(new Option("\tPrint the individual trees in the output", "print", 0, "-print"));

        /* 417 */newVector.addElement(new Option("\tNumber of execution slots.\n\t(default 1 - i.e. no parallelism)",
                "num-slots", 1, "-num-slots <num>"));

        /* 422 */Enumeration enu = super.listOptions();
        /* 423 */while (enu.hasMoreElements()) {
            /* 424 */newVector.addElement(enu.nextElement());
        }

        /* 427 */return newVector.elements();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public String[] getOptions()
    {
        /* 440 */Vector result = new Vector();

        /* 442 */result.add("-I");
        /* 443 */result.add("" + getNumTrees());

        /* 445 */result.add("-K");
        /* 446 */result.add("" + getNumFeatures());

        /* 448 */result.add("-S");
        /* 449 */result.add("" + getSeed());

        /* 451 */if (getMaxDepth() > 0) {
            /* 452 */result.add("-depth");
            /* 453 */result.add("" + getMaxDepth());
        }

        /* 456 */if (this.m_printTrees) {
            /* 457 */result.add("-print");
        }

        /* 460 */result.add("-num-slots");
        /* 461 */result.add("" + getNumExecutionSlots());

        /* 463 */String[] options = super.getOptions();
        /* 464 */for (int i = 0; i < options.length; i++) {
            /* 465 */result.add(options[i]);
        }
        /* 467 */return (String[]) result.toArray(new String[result.size()]);
    }

    public void setOptions(String[] options)
            throws Exception
    {
        /* 509 */String tmpStr = Utils.getOption('I', options);
        /* 510 */if (tmpStr.length() != 0) {
            /* 511 */this.m_numTrees = Integer.parseInt(tmpStr);
        } else {
            /* 513 */this.m_numTrees = 10;
        }

        /* 516 */tmpStr = Utils.getOption('K', options);
        /* 517 */if (tmpStr.length() != 0) {
            /* 518 */this.m_numFeatures = Integer.parseInt(tmpStr);
        } else {
            /* 520 */this.m_numFeatures = 0;
        }

        /* 523 */tmpStr = Utils.getOption('S', options);
        /* 524 */if (tmpStr.length() != 0) {
            /* 525 */setSeed(Integer.parseInt(tmpStr));
        } else {
            /* 527 */setSeed(1);
        }

        /* 530 */tmpStr = Utils.getOption("depth", options);
        /* 531 */if (tmpStr.length() != 0) {
            /* 532 */setMaxDepth(Integer.parseInt(tmpStr));
        } else {
            /* 534 */setMaxDepth(0);
        }

        /* 537 */setPrintTrees(Utils.getFlag("print", options));

        /* 539 */tmpStr = Utils.getOption("num-slots", options);
        /* 540 */if (tmpStr.length() > 0) {
            /* 541 */setNumExecutionSlots(Integer.parseInt(tmpStr));
        } else {
            /* 543 */setNumExecutionSlots(1);
        }

        /* 546 */super.setOptions(options);

        /* 548 */Utils.checkForRemainingOptions(options);
    }

    public Capabilities getCapabilities()
    {
        /* 557 */return new REPTree().getCapabilities();
    }

    public void buildClassifier(Instances data)
            throws Exception
    {
        /* 569 */getCapabilities().testWithFail(data);

        /* 572 */data = new Instances(data);
        /* 573 */data.deleteWithMissingClass();

        /* 575 */this.m_bagger = new Bagging();
        /* 576 */REPTree rTree = new REPTree();

        /* 579 */this.m_KValue = this.m_numFeatures;
        /* 580 */if (this.m_KValue < 1)
            this.m_KValue = ((int) Utils.log2(data.numAttributes()) + 1);
        // /* 581 */ rTree.setKValue(this.m_KValue);
        /* 582 */rTree.setMaxDepth(getMaxDepth());

        /* 585 */this.m_bagger.setClassifier(rTree);
        /* 586 */this.m_bagger.setSeed(this.m_randomSeed);
        /* 587 */this.m_bagger.setNumIterations(this.m_numTrees);
        /* 588 */this.m_bagger.setCalcOutOfBag(true);
        /* 589 */this.m_bagger.setNumExecutionSlots(this.m_numExecutionSlots);
        /* 590 */this.m_bagger.buildClassifier(data);
    }

    public double[] distributionForInstance(Instance instance)
            throws Exception
    {
        /* 602 */return this.m_bagger.distributionForInstance(instance);
    }

    public String toString()
    {
        /* 612 */if (this.m_bagger == null) {
            /* 613 */return "Random forest not built yet";
        }
        /* 615 */StringBuffer temp = new StringBuffer();
        /* 616 */temp.append("Random forest of " + this.m_numTrees + " trees, each constructed while considering "
                + this.m_KValue + " random feature" + (this.m_KValue == 1 ? "" : "s") + ".\n" + "Out of bag error: "
                + Utils.doubleToString(this.m_bagger.measureOutOfBagError(), 4) + "\n"
                + (getMaxDepth() > 0 ? "Max. depth of trees: " + getMaxDepth() + "\n" : "") + "\n");

        /* 623 */if (this.m_printTrees) {
            /* 624 */temp.append(this.m_bagger.toString());
        }
        /* 626 */return temp.toString();
    }

    public void generatePartition(Instances data)
            throws Exception
    {
        /* 635 */buildClassifier(data);
    }

    public double[] getMembershipValues(Instance inst)
            throws Exception
    {
        /* 643 */return this.m_bagger.getMembershipValues(inst);
    }

    public int numElements()
            throws Exception
    {
        /* 651 */return this.m_bagger.numElements();
    }

    public String getRevision()
    {
        /* 660 */return RevisionUtils.extract("$Revision: 9186 $");
    }

    public static void main(String[] argv)
    {
        /* 669 */runClassifier(new RandomForest(), argv);
    }
}
