package net.tknika.domo.service.thermo;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import net.tknika.domo.Config;
import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.thermo.task.RoomTask;
import net.tknika.domo.service.thermo.task.ThermoTask;

import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.apache.log4j.Logger;

public class ThermodomoticService {

    private static final Logger logger = Logger.getLogger(ThermodomoticService.class);
    private static final String THREAD_NAME = "thermo-service";

    private static ThermodomoticService instance;

    private String previousThreadName;
    private ScheduledThreadPoolExecutor executor;

    private int minWorkers;
    private int maxWorkers;
    private String idleTime;

    public static ThermodomoticService getInstance() {
        if (instance == null)
            instance = new ThermodomoticService();

        return instance;
    }

    public ThermodomoticService() {

        try {
            Properties p = new Properties();
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            try {
                minWorkers = Integer.parseInt(p.getProperty("thermo.workers.min"));
            } catch (Exception e) {
                minWorkers = 1;
            }
            try {
                maxWorkers = Integer.parseInt(p.getProperty("thermo.workers.max"));
            } catch (Exception e) {
                maxWorkers = 20;
            }

            idleTime = p.getProperty("thermo.workers.idleTime");
            if (idleTime == null)
                idleTime = "30 seconds";

        } catch (IOException e) {
            logger.error("ThermodomoticService constructor" + e.getMessage());
        }
    }

    // private DelayQueue<RoomTask> roomTasks;

    public void start() {
        previousThreadName = Thread.currentThread().getName();
        Thread.currentThread().setName(THREAD_NAME);
        try {
            logger.info("Starting...");
            // TODO check all services
            boolean allServicesOK = true;
            if (!allServicesOK) {
                logger.error("All services not OK");
                System.exit(1);
            }

            executor = new ScheduledThreadPoolExecutor(minWorkers);
            executor.setMaximumPoolSize(maxWorkers);
            String[] idleTimeSplitted = idleTime.split(" ");

            long keepAliveTime = Long.parseLong(idleTimeSplitted[0]);
            TimeUnit keepAliveTimeUnit = TimeUnit.valueOf(idleTimeSplitted[1].toUpperCase());

            executor.setKeepAliveTime(keepAliveTime, keepAliveTimeUnit);
            executor.allowCoreThreadTimeOut(true);
            logger.info("Started");
        } catch (Exception e) {
            logger.error("Error starting", e);
            Thread.currentThread().setName(previousThreadName);
        }
    }

    public void stop() {
        logger.info("Stoping...");
        if (executor != null) {
            // stop tasks
            try {
                logger.info("Terminating thermodomotic service tasks");
                executor.shutdown();
                if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                    executor.shutdownNow();
                    if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                        logger.error("Not possible to terminate tasks gracefully." +
                                " Thermodomotic service may not have terminated successfully");
                    } else {
                        logger.info("Tasks terminated");
                    }
                } else {
                    logger.info("Tasks terminated");
                }
            } catch (InterruptedException ie) {
                executor.shutdownNow();
                Thread.currentThread().interrupt();
            }
        }
        logger.info("Stopped");
        Thread.currentThread().setName(previousThreadName);
    }

    public ScheduledFuture<?> execute(ThermoTask task) {
        if (!executor.isShutdown() && !executor.isTerminating() && !executor.isTerminated()) {
            Calendar cal = Calendar.getInstance();
            logger.info(task.getClass().getSimpleName() + " - " + task.getExecution());
            if (task.getExecution() == null || (task.getExecution().getTime() - cal.getTimeInMillis() < 0)) {
                return executor.schedule(task, 0, TimeUnit.MILLISECONDS);
            } else {
                return executor.schedule(task, task.getExecution().getTime() - cal.getTimeInMillis(),
                        TimeUnit.MILLISECONDS);
            }
        } else {
            logger.info("denied " + task.getClass().getSimpleName() + " execution because executor not available");
            return null;
        }
    }

    public void shutDown() {
        logger.info("shutting down executor");
        executor.shutdownNow();
    }

    public void reanimate() {
        logger.info("create new executor");
        executor = new ScheduledThreadPoolExecutor(minWorkers);
        executor.setMaximumPoolSize(maxWorkers);
        String[] idleTimeSplitted = idleTime.split(" ");
        long keepAliveTime = Long.parseLong(idleTimeSplitted[0]);
        TimeUnit keepAliveTimeUnit = TimeUnit.valueOf(idleTimeSplitted[1].toUpperCase());
        executor.setKeepAliveTime(keepAliveTime, keepAliveTimeUnit);
        executor.allowCoreThreadTimeOut(true);
    }

    @SuppressWarnings("unchecked")
    public <T extends RoomTask> T _newTask(Class<? extends RoomTask> taskClass, Date execution, Room room,
            ExecutionPriority priority) {
        T task = (T) BeanProvider.getContextualReference(taskClass, false);
        task.setExecution(execution);
        task.setRoom(room);
        task.setPriority(priority);
        return task;
    }

    @SuppressWarnings("unchecked")
    public <T extends RoomTask> T _newTask(Class<? extends RoomTask> taskClass, Date execution, Room room) {
        T task = (T) BeanProvider.getContextualReference(taskClass, false);
        task.setExecution(execution);
        task.setRoom(room);
        task.setPriority(ExecutionPriority.NORMAL);
        return task;
    }

    @SuppressWarnings("unchecked")
    public <T extends RoomTask> T _newTask(Class<? extends RoomTask> taskClass, Room room) {
        T task = (T) BeanProvider.getContextualReference(taskClass, false);
        task.setExecution(new Date());
        task.setRoom(room);
        task.setPriority(ExecutionPriority.NORMAL);
        return task;
    }

}
