package net.tknika.domo.service.thermo.task;

import java.util.Date;

import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.device.DeviceService;
import net.tknika.domo.service.device.zwave.OpenhabServiceImpl;

import org.apache.log4j.Logger;

public class TurnOnAllExceptHeat extends RoomTask {

    private static final String THREAD_NAME_SUFFIX = "turn-on-all-except-heat";
    private static final Logger logger = Logger.getLogger(TurnOnAllExceptHeat.class);

    private Reservation reservation;
    private long trainingDataStartTime;

    public TurnOnAllExceptHeat() {
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public long getTrainingDataStartTime() {
        return trainingDataStartTime;
    }

    public void setTrainingDataStartTime(long trainingDataStartTime) {
        this.trainingDataStartTime = trainingDataStartTime;
    }

    public TurnOnAllExceptHeat(Room room, Date execution, ExecutionPriority priority, Reservation reservation,
            long tdStartTime) {
        super(room, execution, priority);
        this.reservation = reservation;
        this.trainingDataStartTime = tdStartTime;
    }

    public TurnOnAllExceptHeat(Room room, Date execution) {
        super(room, execution);
    }

    public TurnOnAllExceptHeat(Room room, Date execution, Reservation reservation, long tdStartTime) {
        super(room, execution);
        this.reservation = reservation;
        this.trainingDataStartTime = tdStartTime;
    }

    public TurnOnAllExceptHeat(Reservation reservation, long tdStartTime) {
        super(reservation.getRoom());
        this.reservation = reservation;
        this.trainingDataStartTime = tdStartTime;
    }

    @Override
    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    @Override
    public void execute() {
        logger.info("executing turn on all except heat");
        try {
            DeviceService deviceService = new OpenhabServiceImpl(); // BeanProvider.getContextualReference(DeviceService.class);
            deviceService.turnOnAllExceptHeat(getRoom());
        } catch (Exception e) {
            logger.error("terminated turn on all except heating with error", e);
        }
    }
}
