package net.tknika.domo.service.thermo.task.check;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import net.tknika.domo.Config;
import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.ExecutionPriority;
import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.Room;
import net.tknika.domo.service.thermo.task.RoomTask;
import net.tknika.domo.service.thermo.task.TurnOffHeating;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

public class StopHeating extends RoomTask {

    private static final Logger logger = Logger.getLogger(StopHeating.class);
    private static final String THREAD_NAME_SUFFIX = "stop-heating";

    private Reservation reservation;
    int desiredTemperature;
    int timeIntervalInMinutes;

    private long trainingDataStartTime;

    public StopHeating(Room room, Reservation reservation, long tdStart) {
        super(room);
        this.trainingDataStartTime = tdStart;
        init();
    }

    public StopHeating(Room room, Date execution, ExecutionPriority priority, Reservation reservation, long tdStart) {
        super(room, execution, priority);
        this.trainingDataStartTime = tdStart;
        init();
    }

    public StopHeating(Room room, Date execution) {
        super(room, execution);
        init();
    }

    public StopHeating(Room room, Date execution, Reservation reservation, long tdStart) {
        super(room, execution);
        this.trainingDataStartTime = tdStart;
        init();
    }

    public StopHeating() {
        init();
    }

    private void init() {
        Properties p = new Properties();
        try {
            p.load(getClass().getClassLoader().getResourceAsStream(Config.File));
            try {
                desiredTemperature = Integer.parseInt(p.getProperty("domotic.desiredTemperature"));
            } catch (Exception e) {
            }
            try {
                timeIntervalInMinutes = Integer.parseInt(p.getProperty("domotic.tempCheckingTimeIntervalInMinutes"));
            } catch (Exception e) {
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public long getTrainingDataStartTime() {
        return trainingDataStartTime;
    }

    public void setTrainingDataStartTime(long trainingDataStartTime) {
        this.trainingDataStartTime = trainingDataStartTime;
    }

    @Override
    protected String getThreadNameSuffix() {
        return THREAD_NAME_SUFFIX;
    }

    @Override
    protected void execute() {
        logger.info("executing task to check if desired temp has been reached");
        try {
            DomoticData data = deviceService.getRoomData(reservation.getRoom());
            if (data != null) {
                logger.info("Current temperature: " + data.getTemperature());
                if (data.getTemperature() >= desiredTemperature) {
                    TurnOffHeating turnOff = new TurnOffHeating(getRoom(), new Date()); // .newTask(TurnOffHeating.class,
                                                                                        // new Date(), getRoom());
                    turnOff.setReservation(reservation);
                    turnOff.setTrainingDataStartTime(trainingDataStartTime);
                    thermodomoticService.execute(turnOff);
                } else {
                    Date date = DateUtils.addMinutes(getExecution(), timeIntervalInMinutes);
                    StopHeating repeatTask = new StopHeating(getRoom(), date); // thermodomoticService.newTask(StopHeating.class,
                                                                               // date, getRoom());
                    repeatTask.setReservation(reservation);
                    repeatTask.setTrainingDataStartTime(trainingDataStartTime);
                    thermodomoticService.execute(repeatTask);
                }
            }
        } catch (Exception e) {
            logger.error("Terminated with errors", e);
        }
    }
}
