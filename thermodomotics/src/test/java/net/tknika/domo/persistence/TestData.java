package net.tknika.domo.persistence;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.tknika.domo.model.Room;
import net.tknika.domo.model.TrainingData;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import au.com.bytecode.opencsv.CSVReader;

@Singleton
public class TestData {

    @Inject
    ThermodomoticsDAO dao;

    public void load() {
        List<Room> rooms = dao.getRooms();
        if ((rooms == null) || rooms.isEmpty()) {
            loadRooms();
        }
        readData();
    }

    private void loadRooms() {
        dao.deleteRooms();
        Room room = new Room();
        room.setCode("room 1105");
        room.setName("Aula 1105");
        dao.save(room);
        room = new Room();
        room.setCode("room 1207");
        room.setName("Aula 1207");
        dao.save(room);
    }

    @SuppressWarnings("deprecation")
    public void readData() {
        try {
            CSVReader reader = new CSVReader(new FileReader("training_data.csv"), ';');
            String[] nextLine;
            // read next to skip first line with column names
            reader.readNext();
            while ((nextLine = reader.readNext()) != null) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Long.valueOf(nextLine[1]));
                TrainingData data = new TrainingData(dao.getRoom(nextLine[0]), cal.getTimeInMillis());
                cal.setTimeInMillis(Long.valueOf(nextLine[2]));
                data.setEnd(cal.getTimeInMillis());
                data.setInitialTemperature(Double.valueOf(nextLine[3]));
                data.setFinalTemperature(Double.valueOf(nextLine[4]));
                data.setExternalTemperature(Double.valueOf(nextLine[5]));
                TrainingData readedTrainingData = dao.getTrainingData(data.getRoom(), new Date(data.getStart()));
                if (readedTrainingData != null) {
                    continue;
                } else {
                    Logger.getLogger(TestData.class).log(Priority.INFO,
                            data.getRoom().getCode() + "/" + data.getStart());
                    dao.save(data);
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
