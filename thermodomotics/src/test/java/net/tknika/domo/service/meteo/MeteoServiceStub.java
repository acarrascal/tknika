package net.tknika.domo.service.meteo;

import java.util.Date;

import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;

import net.tknika.domo.model.MeteoData;
import net.tknika.domo.service.meteo.MeteoService;

@Alternative
@Singleton
public class MeteoServiceStub implements MeteoService {

    @Override
    public MeteoData getObservation(Date datetime) {
        MeteoData observation = new MeteoData(datetime);
        observation.setTemperature(17.3);
        observation.setHumidity(83.8);
        observation.setRadiation(10.5);
        return observation;
    }

    @Override
    public MeteoData getForecast(Date datetime) {
        MeteoData forecast = new MeteoData(datetime);
        forecast.setTemperature(19.2);
        forecast.setHumidity(87.2);
        forecast.setRadiation(8.5);
        return forecast;
    }

}
