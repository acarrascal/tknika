package net.tknika.domo.service.thermo;

import java.util.List;

import javax.inject.Inject;

import net.tknika.domo.model.Reservation;
import net.tknika.domo.model.Room;
import net.tknika.domo.model.ThermoCurve;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.service.reservation.ReservationService;
import net.tknika.domo.service.thermo.task.check.GetCurveData;

import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiTestRunner.class)
public class RoomTest {

    private Logger logger = Logger.getLogger(RoomTest.class);

    @Inject
    private ThermodomoticService service;
    @Inject
    ThermodomoticsDAO dao;
    @Inject
    private ReservationService reservationService;

    private List<Room> rooms;
    private List<Reservation> reservations;

    @Before
    public void loadTestData() {
        reservations = reservationService.getReservations();
        rooms = dao.getRooms();
    }

    @Test
    public void heatingTest() {
        logger.debug(rooms.size());
        logger.debug(reservations.size());
        // for (Room room : rooms) {
        // deviceService.turnOnHeating(room);
        // deviceService.turnOffHeating(room);
        // }
    }

    @Test
    public void createCurve() {
        try {
            service.start();
            for (Room room : rooms) {
                ThermoCurve curve = dao.getCurve(room);
                if (curve == null) {
                    GetCurveData task;
                    for (int i = 0; i < 10; i++) {
                        task = new GetCurveData(room); // .newTask(GetCurveData.class, room);
                        service.execute(task);
                    }
                }
            }
            System.in.read();
            for (Room room : rooms) {
                Assert.assertNotNull(dao.getCurve(room));
            }
            service.stop();
        } catch (Exception e) {
            logger.error(e);
        }
    }

    // @Test
    // public void firstReservationTest() {
    // for (Reservation rsv : reservations) {
    // boolean isFirstReservation = reservationService.getReservations(rsv.getRoom()).indexOf(rsv) < 1;
    // List<Reservation> roomReservations = reservationService.getReservations(rsv.getRoom());
    // Reservation firstReservation = roomReservations.get(0);
    // for (Reservation roomRsv : roomReservations) {
    // if (roomRsv.getStart().after(roomRsv.getStart())) {
    // firstReservation = roomRsv;
    // }
    // }
    // Assert.assertEquals(isFirstReservation, rsv.equals(firstReservation));
    // }
    // }

}
