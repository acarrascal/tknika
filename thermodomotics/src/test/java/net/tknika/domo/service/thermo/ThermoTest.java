package net.tknika.domo.service.thermo;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.inject.Inject;

import net.tknika.domo.model.DomoticData;
import net.tknika.domo.model.Room;
import net.tknika.domo.persistence.ThermodomoticsDAO;
import net.tknika.domo.service.device.DeviceService;
import net.tknika.domo.service.device.zwave.ZWaveService;
import net.tknika.domo.service.reservation.ReservationService;
import net.tknika.domo.service.reservation.gcal.GoogleCalendarService;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiTestRunner.class)
public class ThermoTest {

    private Logger logger = Logger.getLogger(ThermoTest.class);

    @Inject
    private ThermodomoticService service;
    @Inject
    @ZWaveService
    private DeviceService deviceService;
    @Inject
    @GoogleCalendarService
    private ReservationService reservationService;
    @Inject
    private ThermodomoticsDAO dao;

    @Before
    public void setUp() {
        reservationService.getReservations();
        service.start();
    }

    @Test
    public void getDataTest() {
        DomoticData data = deviceService.getRoomData(null);
        assertTrue(DateUtils.isSameDay(new Date(), data.getDatetime()));
        logger.info(data.getDatetime());
        logger.info(data.getTemperature());
    }

    @Test
    public void voidTest() {
        //
    }

    @Test
    public void heatTest() {
        Room room = dao.getRoom("zwave1");
        deviceService.turnOnHeating(room);
        deviceService.turnOffHeating(room);
    }

    @Test
    public void getCurve() throws InterruptedException {
        // Room room = dao.getRoom("zwave1");
        // Reservation reservation = new Reservation();
        // reservation.setDescription("bla bla");
        // reservation.setRoom(room);
        // reservation.setStart(DateUtils.addHours(new Date(), 2));
        // reservation.setEnd(DateUtils.addHours(reservation.getStart(), 1));
        // // reservationService.makeReservation(reservation);
        // Date turnOnStart = new Date();
        // TurnOnHeating turnOnHeating = service.newTask(TurnOnHeating.class, turnOnStart, room);
        // turnOnHeating.setReservation(reservation);
        // service.execute(turnOnHeating);
        // Date curveDate = new Date();
        // Date endCurveData = DateUtils.addSeconds(new Date(), 8);
        // GetCurveData curveDataTask;
        // while (curveDate.before(endCurveData)) {
        // curveDataTask = service.newTask(GetCurveData.class, curveDate, room);
        // service.execute(curveDataTask);
        // curveDate = DateUtils.addSeconds(curveDate, 2);
        // }
        // TurnOn turnOn = service.newTask(TurnOn.class, curveDate, reservation.getRoom());
        // turnOn.setReservation(reservation);
        // service.execute(turnOn);
        // TurnOffHeating turnOffHeating = service.newTask(TurnOffHeating.class, curveDate, room);
        // turnOffHeating.setReservation(reservation);
        // turnOffHeating.setTrainingDataStartTime(turnOnStart.getTime());
        // service.execute(turnOffHeating);
        // Thread.sleep(curveDate.getTime() - Calendar.getInstance().getTimeInMillis());
    }

    @After
    public void tearDown() {
        service.stop();
    }

}
