package net.tknika.domo.service.thermo;

import java.util.Date;

import javax.inject.Inject;

import net.tknika.domo.model.Room;
import net.tknika.domo.service.reservation.ReservationService;
import net.tknika.domo.service.thermo.task.TurnOnHeating;

import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiTestRunner.class)
public class ThermodomoticServiceTest {

    @Inject
    private ThermodomoticService service;
    @Inject
    private ReservationService rs;

    @Test
    public void testService() {
        service.start();
        Room room = new Room();
        room.setCode("1207");
        room.setName("Aula 1207: Laboratorio de domótica");
        service.execute(new TurnOnHeating(room, new Date()));
        service.stop();
    }

}
